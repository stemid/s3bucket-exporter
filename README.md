# AWS S3Bucket exporter for Prometheus

None of the S3 exporters I found worked the way I wanted.

The s3bucket plugin simply fetches all the buckets it can access and adds up their total size. More plugins can be added to do other things.

## Technical design

This is based on another prometheus exporter so it's actually plugin based.

* Plugins are installed into ``reportlib/plugins`` as python modules.
* ``reportlib/server.py`` loads and imports all the plugin modules.
* It expects to find an ExportPlugin class based on the example in the sample plugin.
* It then expects to run ExportPlugin.update() method periodically to refresh metrics from the source.
* And finally publishes those metrics through the ``prometheus_client`` python library.
* All this is run using a Twisted.internet.reactor TCP server to serve the metrics.

## Install

### Virtualenv pip packages

  python3 -m venv .venv
  source .venv/bin/activate
  pip install -r requirements.txt

## Run in virtualenv

  python -m reportlib.server --help
  python -m reportlib.server

## Environment config

* ``S3BUCKET_EXPORTER_LISTEN_HOST`` instead of ``--host``, defaults to localhost.
* ``S3BUCKET_EXPORTER_LISTEN_PORT`` instead of ``--port``, defaults to 9030.

## s3bucket plugin config

This plugin takes all its config from environment variables.

* ``AWS_ACCESS_KEY_ID`` - AWS access key ID.
* ``AWS_SECRET_ACCESS_KEY`` - AWS secret access key.
