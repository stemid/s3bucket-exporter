#!/usr/bin/env python3

from setuptools import setup, find_packages

setup_kwargs = {
    'name': 's3bucket-exporter',
    'version': '0.0.1',
    'description': 'Prometheus AWS S3 Bucket exporter',
    'author': 'Stefan Midjich',
    'author_email': 'swehack@gmail.com',
    'url': 'https://gitlab.com/stemid/s3bucket-exporter',
    'license': 'ISC',
    #'zip_safe': False, # This causes pickle to fail for rq
    'packages': [
        'reportlib'
    ],
    'entry_points': {
        'console_scripts': [
            'exporterd = reportlib.server:run_daemon'
        ]
    },
    'scripts': [],
    'data_files': [],
    'options': {},
}

setup(**setup_kwargs)
