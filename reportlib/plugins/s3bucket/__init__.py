from pprint import pprint as pp

import boto3
from botocore.config import Config
from prometheus_client import Gauge


class ExportPlugin(object):

    def __init__(self):
        config = Config(
            connect_timeout=10, read_timeout=10,
            retries={'max_attempts': 10}
        )
        self.s3 = boto3.resource('s3', config=config)
        self.s3bucket_total_bytes = Gauge(
            's3bucket_total_bytes',
            'S3 bucket total size in bytes',
            ['bucket']
        )
        self.s3bucket_total_keys = Gauge(
            's3bucket_total_keys',
            'S3 bucket total number of keys',
            ['bucket']
        )


    def update(self):
        for bucket in self.s3.buckets.all():
            bytes_total = 0
            keys_total = 0

            for key in bucket.objects.all():
                keys_total += 1
                bytes_total += key.size

            self.s3bucket_total_bytes.labels(
                bucket=bucket.name
            ).set(bytes_total)
            self.s3bucket_total_keys.labels(
                bucket=bucket.name
            ).set(keys_total)
