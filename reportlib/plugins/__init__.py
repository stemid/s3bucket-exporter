from importlib import import_module
from pkgutil import iter_modules
from configparser import NoSectionError

from reportlib import APP_NAME

class PluginManager(object):

    def __init__(self):
        self.plugins = {}
        self.plugins.update(
            self._find_plugins(import_module('reportlib.plugins'))
        )


    def _find_plugins(self, ns):
        ret_data = {}
        for f, name, ispkg in iter_modules(ns.__path__, ns.__name__+'.'):
            if not ispkg:
                continue
            ret_data.update({
                name: {
                    'plugin_ns': import_module(name),
                    'name': name
                }
            })
        return ret_data


