from os import environ

try:
    from dotenv import load_dotenv
    load_dotenv(verbose=False)
except ImportError:
    pass

import click
from prometheus_client.twisted import MetricsResource
from twisted.web.server import Site
from twisted.web.resource import Resource
from twisted.internet import reactor
from twisted.internet.task import LoopingCall

from reportlib import APP_NAME
from reportlib.logger import AppLogger
from reportlib.plugins import PluginManager


class ServerDaemon(object):

    def __init__(self, **kw):
        self.listen_port = int(kw.get('listen_port'))
        self.listen_host = kw.get('listen_host')

        al = AppLogger(name=APP_NAME)
        self.l = al.logger

        self._updates = []


    def run(self):
        pm = PluginManager()
        for name, data in pm.plugins.items():
            plugin = data.get('plugin_ns')
            self.l.info('Loading plugin:{plugin}'.format(
                plugin=name
            ))

            try:
                p = plugin.ExportPlugin()
            except Exception as e:
                self.l.error('Failed loading plugin:{plugin}: {error}'.format(
                    plugin=name,
                    error=str(e)
                ))
                continue

            self._updates.append({
                'plugin': name,
                'update_func': p.update
            })

            loop = LoopingCall(
                self.update
            )
            loop.start(10)

        root = Resource()
        root.putChild(b'metrics', MetricsResource())

        factory = Site(root)
        reactor.listenTCP(
            port=self.listen_port,
            interface=self.listen_host,
            factory=factory
        )

        base_url = 'http://{hostname}:{port}'.format(
            hostname=self.listen_host,
            port=self.listen_port
        )

        self.l.info('Serving metrics on {base_url}/metrics'.format(
            base_url=base_url
        ))

        reactor.run()


    def update(self):
        for u in self._updates:
            self.l.debug('Updating metrics for plugin:{plugin}'.format(
                plugin=u.get('plugin', 'UNKNOWN')
            ))
            try:
                u.get('update_func')()
            except Exception as e:
                self.l.error('Failed for plugin:{plugin}: {error}'.format(
                    plugin=u.get('plugin', 'UNKNOWN'),
                    error=str(e)
                ))
                continue


@click.command()
@click.option(
    '--port', 'listen_port',
    default=lambda: environ.get('S3BUCKET_EXPORTER_LISTEN_PORT', 9030),
    metavar='<int>',
    help='Environment: S3BUCKET_EXPORTER_LISTEN_PORT, Default:9030'
)
@click.option(
    '--host', 'listen_host',
    default=lambda: environ.get('S3BUCKET_EXPORTER_LISTEN_HOST', 'localhost'),
    metavar='<str>',
    help='Environment: S3BUCKET_EXPORTER_LISTEN_HOST, Default:localhost'
)
def run_daemon(**kw):
    daemon = ServerDaemon(
        **kw
    )
    daemon.run()

if __name__ == '__main__':
    exit(run_daemon(None, None))
