import sys
from os import environ

from logging import StreamHandler
from logging import Formatter, getLogger, getLevelName, DEBUG, WARN, INFO
from logging.handlers import SysLogHandler, RotatingFileHandler


class AppLogger(object):

    def __init__(self, name):
        """This initializes with a RawConfigParser object to get most of its
        default settings.
        """

        formatter = Formatter('%(asctime)s %(name)s[%(process)s] %(levelname)s: %(message)s')
        self.logger = getLogger(name)

        h = StreamHandler(stream=sys.stdout)

        h.setFormatter(formatter)
        self.logger.addHandler(h)

        try:
            log_level = getLevelName(
                environ.get('LOG_LEVEL', 'INFO')
            )
        except:
            log_level = DEBUG
            pass

        self.log_level = log_level
        self.handler = h
        self.logger.setLevel(log_level)


    def apply_handler(self, logger_name):
        """
        Some frameworks init their own loggers, like tornado for example.
        So use this to apply your handler to their loggers by specifying the
        logger name.
        """
        logger = getLogger(logger_name)
        logger.addHandler(self.handler)

